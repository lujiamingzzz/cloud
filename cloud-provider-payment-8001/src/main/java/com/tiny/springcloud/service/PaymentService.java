package com.tiny.springcloud.service;

import com.tiny.springcloud.entities.Payment;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
public interface PaymentService {
    /**
     * 保存订单
     * @param payment
     * @return
     */
    int save(Payment payment);

    /**
     *  查询订单
     * @param id
     * @return
     */
    Payment getPaymentById(Integer id);
}
