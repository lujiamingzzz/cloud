package com.tiny.springcloud.controller;

import com.tiny.springcloud.entities.CommonResult;
import com.tiny.springcloud.entities.Payment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
@RequestMapping("consumer")
@RestController
public class OrderController {
    public final static String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";
//    public final static String PAYMENT_URL = "http://localhost:8001";

    @Resource
    private RestTemplate restTemplate;

    @PostMapping("payment/create")
    public Object create(@RequestBody Payment payment){
        return restTemplate.postForObject(PAYMENT_URL+"/payment/save", payment, CommonResult.class);
    }

    @GetMapping("payment/getPayment/{id}")
    public Object getPayment(@PathVariable Integer id){
        return restTemplate.getForObject(PAYMENT_URL+"/payment/getPaymentById/"+id, CommonResult.class);
    }
}
