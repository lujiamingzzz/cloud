package com.tiny.springcloudalibaba.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.tiny.springcloud.entities.Payment;
import com.tiny.springcloud.response.BaseResponse;
import com.tiny.springcloudalibaba.customSentinelHandle.customHandle;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author lujiaming
 * @Date 2020/5/5 12:49
 * @Description 测试 SentinelResource 注解
 */

@RestController
public class RateLimitController {

    /**
     * 按资源名称限流测试
     * 有兜底方法
     * @return
     */
    @GetMapping("/byResource")
    @SentinelResource(value = "byResource", blockHandler = "handleException")
    public Object byResource() {
        return BaseResponse.success(200, "按资源名称限流测试OK", new Payment(2020, "serial001"));
    }

    /**
     * 兜底方法
     * @param exception
     * @return
     */
    public Object handleException(BlockException exception) {
        return BaseResponse.failture(444, exception.getClass().getCanonicalName() + "\t服务不可用", null);
    }

    /**
     * 按url限流测试
     * 无兜底方法，会使用默认
     * @return
     */
    @GetMapping("/byUrl")
    @SentinelResource(value = "byUrl")
    Object byUrl() {
        return BaseResponse.success(200, "按url限流测试OK", new Payment(2020, "serial002"));
    }

    /**
     * 自定义兜底方法限流处理测试
     * @return
     */
    @GetMapping("/customHandleException")
    @SentinelResource(value = "customHandleException", blockHandlerClass = customHandle.class, blockHandler = "customHandleException")
    Object customHandleException() {
        return BaseResponse.success(200, "自定义兜底方法限流处理测试OK", new Payment(2020, "serial003"));
    }
}
