# cloud1
## 集群：注册中心1 <--相互注册--> 注册中心2
### cloud-eureka-server-7001 ---注册中心1
### cloud-eureka-server-7002 ---注册中心2
### cloud-provider-payment-8001 ---支付模块1
### cloud-provider-payment-8002 ---支付模块2
### cloud-consumer-order80 ---消费者模块
#### 消费者模块由RestTemplate开启@LoadBalanced注解赋予RestTemplate负载均衡能力，轮询访问支付模块1/支付模块2
### common ---公共模块
