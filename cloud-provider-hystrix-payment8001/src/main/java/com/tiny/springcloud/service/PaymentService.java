package com.tiny.springcloud.service;

public interface PaymentService {
    // ======服务降级
    /**
     * 正常访问
     * @param id
     * @return
     */
    String paymentInfo_OK(Integer id);

    String paymentInfo_TimeOut(Integer id);


    // ======服务熔断
    String paymentCircuitBreaker(Integer id);
}
