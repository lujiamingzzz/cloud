package com.tiny.springcloud.controller;

import com.tiny.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import com.tiny.springcloud.entities.Payment;
import com.tiny.springcloud.response.BaseResponse;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/

@RestController
@RequestMapping("payment")
@Slf4j
public class PaymentController {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    @Resource
    private PaymentService paymentService;
    @Resource
    private DiscoveryClient discoveryClient; // 服务发现
    @Value("${server.port}")
    private String serverPort;

    @PostMapping("/save")
    public Object save(@RequestBody Payment payment) {
        int save = paymentService.save(payment);
        if (save > 0) {
            logger.info("插入成功,serverPort：" + serverPort, save);
            return BaseResponse.success(save);
        } else {
            logger.info("插入失败,serverPort：" + serverPort, save);
            return BaseResponse.failture();
        }

    }

    @GetMapping("/getPaymentById/{id}")
    public Object getPaymentById(@PathVariable Integer id) {
        if (id <= 0) {
            return BaseResponse.failture("非法请求");
        }
        return BaseResponse.success(200, serverPort, paymentService.getPaymentById(id));
    }

    @GetMapping(value = "/discovery")
    public Object discovery() {
        // 获得服务列表
        List<String> services = discoveryClient.getServices();
        for (String element:services) {
            logger.info("element:" + element);
        }
        // 获得服务名称对应服务下的所有实例
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance:instances) {
            logger.info("instance:" + instance.getInstanceId() + "\t" + instance.getHost()
            + "\t" + instance.getPort() + "\t" + instance.getUri());
        }
        return this.discoveryClient;
    }
}
