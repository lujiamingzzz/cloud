package com.tiny.springcloud.response;

import com.tiny.springcloud.entities.CommonResult;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
public class BaseResponse {
    public static CommonResult success() {
        return new CommonResult(200, "成功", null);
    }

    public static CommonResult success(int code, String message, Object data) {
        return new CommonResult(code, message, data);
    }

    public static CommonResult success(Object data) {
        return new CommonResult(200, "成功", data);
    }

    public static CommonResult success(String message) {
        return new CommonResult(200, message, null);
    }

    public static CommonResult failture() {
        return new CommonResult(0, "失败", null);
    }

    public static CommonResult failture(String message) {
        return new CommonResult(0, message, null);
    }

    public static CommonResult failture(int code, String message, Object data) {
        return new CommonResult(code, message, data);
    }
}
