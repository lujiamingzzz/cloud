package com.tiny.springcloud.service;

import com.tiny.springcloud.entities.CommonResult;
import com.tiny.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    /**
     *  查询订单
     * @param id
     * @return
     */
    @GetMapping("payment/getPaymentById/{id}")
    Object getPaymentById(@PathVariable("id")Integer id);
}
