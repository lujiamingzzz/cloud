package com.tiny.springcloudalibaba.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlowLimitController {

    @GetMapping("test/A")
    public String testA() {
        return "-------testA";
    }

    @GetMapping("test/B")
    public String testB() {
        return "-------testB";
    }

    @GetMapping("test/C")
    public String testC() {
        return "-------testC";
    }
}
