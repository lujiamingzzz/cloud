package com.tiny.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/

@RestController
@RequestMapping("payment")
public class PaymentController {
    @Resource
    private DiscoveryClient discoveryClient; // 服务发现
    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/zk")
    public Object zk() {
        return "zookeeper:" + serverPort + "\t" + UUID.randomUUID().toString();
    }
}
