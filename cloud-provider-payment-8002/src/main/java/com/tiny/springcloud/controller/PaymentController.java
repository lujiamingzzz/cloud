package com.tiny.springcloud.controller;

import com.tiny.springcloud.entities.Payment;
import com.tiny.springcloud.response.BaseResponse;
import com.tiny.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/

@RestController
@RequestMapping("payment")
@Slf4j
public class PaymentController {
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    @Autowired
    private PaymentService paymentService;
    @Value("${server.port}")
    private String serverPort;

    @PostMapping("/save")
    public Object save(@RequestBody Payment payment) {
        int save = paymentService.save(payment);
        if (save > 0) {
            logger.info("插入成功,serverPort：" + serverPort, save);
            return BaseResponse.success(save);
        } else {
            logger.info("插入失败,serverPort：" + serverPort, save);
            return BaseResponse.failture();
        }

    }

    @GetMapping("/getPaymentById/{id}")
    public Object getPaymentById(@PathVariable Integer id) {
        if (id <= 0) {
            return BaseResponse.failture("非法请求");
        }
        return BaseResponse.success(200, serverPort, paymentService.getPaymentById(id));
    }
}
