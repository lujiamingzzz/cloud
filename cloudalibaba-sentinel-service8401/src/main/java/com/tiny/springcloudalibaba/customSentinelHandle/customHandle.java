package com.tiny.springcloudalibaba.customSentinelHandle;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.tiny.springcloud.response.BaseResponse;

/**
 * @Author lujiaming
 * @Date 2020/5/6 14:47
 * @Description sentinel 自定义blockHandler处理类
 */
public class customHandle {
    public static Object customHandleException(BlockException exception) {
        return BaseResponse.failture(4444, exception.getClass().getCanonicalName() + "\t服务不可用", null);
    }
}
