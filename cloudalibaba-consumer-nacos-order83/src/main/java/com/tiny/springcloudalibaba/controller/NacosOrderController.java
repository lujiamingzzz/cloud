package com.tiny.springcloudalibaba.controller;

import javafx.scene.chart.ValueAxis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RequestMapping("consumer")
@Slf4j
@RestController
public class NacosOrderController {
    @Resource
    private RestTemplate restTemplate;

    @Value("${service-url.nacos-user-service}")
    private String serverUrl;

    @GetMapping(value = "payment/nacos/{id}")
    public String paymentInfo(@PathVariable Integer id) {
        return restTemplate.getForObject(serverUrl + "/nacos/payment/" + id, String.class);
    }
}
