package com.tiny.springcloud.service.impl;

import com.tiny.springcloud.entities.Payment;
import com.tiny.springcloud.mapper.PaymentMapper;
import com.tiny.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentMapper paymentMapper;

    /**
     * 保存订单
     *
     * @param payment
     * @return
     */
    @Override
    public int save(Payment payment) {
        return paymentMapper.save(payment);
    }

    /**
     * 查询订单
     *
     * @param id
     * @return
     */
    @Override
    public Payment getPaymentById(Integer id) {
        return paymentMapper.getPaymentById(id);
    }
}
