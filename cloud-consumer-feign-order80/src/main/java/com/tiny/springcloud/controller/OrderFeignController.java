package com.tiny.springcloud.controller;

import com.tiny.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/

@RestController
@RequestMapping("payment")
@Slf4j
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;
    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/getPaymentById/{id}")
    public Object getPaymentById(@PathVariable("id") Integer id) {
        return paymentFeignService.getPaymentById(id);
    }

}
