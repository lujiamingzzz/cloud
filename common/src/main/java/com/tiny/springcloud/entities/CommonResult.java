package com.tiny.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T>{
    private Integer code;
    private String message;
    private T data;

    public CommonResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = (T) data;
    }
}
