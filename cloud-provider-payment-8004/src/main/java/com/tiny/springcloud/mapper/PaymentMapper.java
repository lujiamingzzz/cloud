package com.tiny.springcloud.mapper;


import org.apache.ibatis.annotations.Mapper;
import com.tiny.springcloud.entities.Payment;

/**
 * @Author lujiaming
 * @Date 2020/4/12
 * @Description
 **/
@Mapper
public interface PaymentMapper {
    /**
     *
     * @param payment
     * @return
     */
    int save(Payment payment);

    /**
     *
     * @param id
     * @return
     */
    Payment getPaymentById(Integer id);
}
