package com.tiny.springcloud.service;

public interface IMessageProvider {
    String send();
}
